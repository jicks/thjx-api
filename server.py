#! /usr/bin/env python2

import json, socket, sys, random

def sendJSON(conn, datas):
    print "Sending: ", datas
    conn.send(json.dumps(datas).encode('ascii'))

def recvJSON(conn, expectedType):
    data = ""
    keepGoing = True
    while keepGoing:
        try:
            keepGoing = False
            data += conn.recv(4096)
            obj = json.loads(data)
        except ValueError:
            keepGoing = True
    print "Received: ", obj
    if obj["type"] == expectedType:
        return obj
    return None

def r(start, end):
    return lambda: random.randint(start, end)

# Socket stuff
s = socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((sys.argv[1], int(sys.argv[2])))
s.listen(2)
s1 = s.accept()[0]
s2= s.accept()[0]

# Accepting the connection
pl1 = recvJSON(s1, "connect")["login"]
pl2 = recvJSON(s2, "connect")["login"]

# Setting up the game
strategies = {"W":0, "S":1, "G":2, "E":3, "M":4, "T":5, "N":6}
strategiesCost = {"W":5, "S":7, "G":7, "E":8, "M":8, "T":12, "N":30}
matrix = [[r(1,6), r(1,10), r(1,6), r(1,12), r(1,10), r(1,3), r(1,4)],
          [r(1,6), r(1,6), r(1,3), r(1,6), r(1,6), r(1,12), r(1,4)],
          [r(1,6), r(1,12), r(1,6), r(1,4), r(1,6), r(1,10), r(1,3)],
          [r(1,3), r(1,4), r(1,6), r(1,6), r(1,12), r(1,4), r(1,6)],
          [r(1,10), r(1,6), r(1,12), r(1,3), r(1,6), r(1,6), r(1,4)],
          [r(1,6), r(1,4), r(1,10), r(1,10), r(1,6), r(1,3), r(1,12)],
          [r(1,12), r(1,10), r(1,10), r(1,3), r(1,4), r(1,6), r(1,6)]]
sendJSON(s1, {"type": "game", "you": pl1, "challenger": pl2})
sendJSON(s2, {"type": "game", "you": pl2, "challenger": pl1})

p1 = recvJSON(s1, "select")["strategies"]
state1 = {"score": 0, "strategies": p1}
p2 = recvJSON(s2, "select")["strategies"]
state2 = {"score": 0, "strategies": p2}

# Sending the results of the select
select_result = {"type": "select_result",
                 "you":{"strategies":p1},
                 "challenger":{"strategies":p2}}
sendJSON(s1, select_result)
select_result["you"]["strategies"] = p2
select_result["challenger"]["strategies"] = p1
sendJSON(s2, select_result)

finished = False
# Game loop
while not finished:
    # Receiving the actions
    a1 = recvJSON(s1, "action")["action_played"]
    a2 = recvJSON(s2, "action")["action_played"]

    # Updating the internal state
    score1 = matrix[strategies[a1]][strategies[a2]]()
    state1["score"] += score1
    state1["action_played"] = a1
    score2 = matrix[strategies[a2]][strategies[a1]]()
    state2["score"] += score2
    state2["action_played"] = a2

    winner = ""
    state1["strategies"][a1] -= 1
    state2["strategies"][a2] -= 1

    if score1 > score2:
        winner = pl1
        state1["strategies"][a1] += 1
    elif score2 > score1:
        winner = pl2
        state2["strategies"][a2] += 1

    if state1["strategies"]["N"] == 0 or state2["strategies"]["N"] == 0:
        finished = True

    # Sending the results
    results = {"type": "results", "ended": finished, "win": False,
            "action_winner": winner, "you": state1, "challenger": state2}
    sendJSON(s1, results)
    results["you"] = state2; results["challenger"] = state1
    sendJSON(s2, results)

    if finished:
        break

    # Receiving the purchase from the players
    purchase1 = recvJSON(s1, "purchase")["strategies"]
    for (key, value) in purchase1.iteritems():
        state1["strategies"][key] += value
        state1["score"] -= value * strategiesCost[key]
    purchase2 = recvJSON(s2, "purchase")["strategies"]
    for (key, value) in purchase2.iteritems():
        state2["strategies"][key] += value
        state2["score"] -= value * strategiesCost[key]
    sendJSON(s1, {"type": "purchase_result", "you": state1, "challenger": state2})
    sendJSON(s2, {"type": "purchase_result", "you": state2, "challenger": state1})

    # Dealing with surrender...
    v1 = recvJSON(s1, "surrender_proposition")
    v2 = recvJSON(s2, "surrender_proposition")
    sendJSON(s1, v2)
    sendJSON(s2, v1)
    finished = recvJSON(s1, "surrender_acceptation")["value"] and v2["value"]
    finished = (recvJSON(s2, "surrender_acceptation")["value"] and v1["value"]) or finished

    results["ended"] = finished
    results["you"] = state1; results["challenger"] = state2
    sendJSON(s1, results)
    results["you"] = state2; results["challenger"] = state1
    sendJSON(s2, results)

    if finished:
        break
