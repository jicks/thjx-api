#! /usr/bin/env python2

import json, socket, hashlib

class API(object):

     # Create the API object and connect to the server at the given address
     # and port.
    def __init__(self, address, port):
        self.socket = socket.create_connection((address, port))

    def __sendJSON(self, datas):
        self.socket.send(json.dumps(datas).encode('ascii'))

     # Send a connect message, with the password as a SHA512 hash.
    def sendConnect(self, login, password):
        token = hashlib.sha512(login + "_thjx_" + password).hexdigest().upper()
        self.__sendJSON({"type": "connect", "login": login, "password": token})

     # Send the selection of strategies to start the game with.
     # The keys should be one of 'W', 'S', 'G', 'E', 'M', 'T', 'N'. The
     # total sum must be equal to 100. It is not verified by the API.
    def sendSelect(self, strategies):
        self.__sendJSON({"type": "select", "strategies": strategies})

     # Send the next action to play. The action should be one of 'W', 'S',
     # 'G', 'E', 'M', 'T', 'N'. It is not verified by the API.
    def sendAction(self, action):
        self.__sendJSON({"type": "action", "action_played": action})

     # Send an order to purchase the asked strategies. The keys should be
     # one of 'W', 'S', 'G', 'E', 'M', 'T', 'N'. It is not verified by the
     # API.
    def sendPurchase(self, strategies):
        self.__sendJSON({"type": "purchase", "strategies": strategies})

     # Send a surrender proposition with the given value.
    def sendSurrenderProposition(self, value):
        self.__sendJSON({"type": "surrender_proposition", "value": value})

     # Send the surrender acceptation to the challenger proposition with
     # the given value.
    def sendSurrenderAcceptation(self, value):
        self.__sendJSON({"type": "surrender_acceptation", "value": value})

     # Send a ready message, stating that you are ready for a new match.
    def sendReady(self):
        self.__sendJSON({"type": "ready"})


    def __recvJSON(self, expectedType):
        data = ""
        keepGoing = True
        while keepGoing:
            try:
                keepGoing = False
                data += self.socket.recv(4096)
                obj = json.loads(data)
            except ValueError:
                keepGoing = True
        if obj["type"] == expectedType:
            return obj
        return None

     # Receive the game message from the server. It only checks if the JSON
     # object has the correct type, and returns None if it is
     # not the case.
    def recvGame(self):
        return self.__recvJSON("game")

     # Receive the select result message from the server. It only checks
     # if the JSON object has the correct type, and returns None if it is not
     # the case.
    def recvSelectResult(self):
        return self.__recvJSON("select_result")

     # Receive the results message from the server. It only checks if the
     # JSON object has the correct type, and returns None if it is not the
     # case.
    def recvResults(self):
        return self.__recvJSON("results")

     # Receive the purchase result message from the server. It only checks
     # if the JSON object has the correct type, and returns None if it is not
     # the case.
    def recvPurchaseResult(self):
        return self.__recvJSON("purchase_result")

     # Receive the challenger surrender proposition from the server.
     # It returns true if the object has the correct type and its value is
     # true.
    def recvSurrenderProposition(self):
        return self.__recvJSON("surrender_proposition")
