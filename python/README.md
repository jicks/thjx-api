What is it?
===========

A simple Python API for the THJX project. It is functional and easy to use, but
it is very limited (for example, there is *NO* data validation).


How to use it?
==============

You just have to import the 'api' module.

The documentation is the comments in 'api.py'. For more information on the
message exchange, please read the full specifications.


Disclaimer
==========

This API is only an example, to help you start your project. You do not have to
use it, you can reimplement it if you wish.

There is *no* warranty of the validity of this API. It has been tested, but
some bugs may remain, they will probably not be fixed.
