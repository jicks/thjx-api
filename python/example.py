#! /usr/bin/env python2

import api, sys

api = api.API(sys.argv[1], sys.argv[2])
api.sendConnect(sys.argv[3], sys.argv[4])

while True:
    game = api.recvGame()
    api.sendSelect({"N": 100})
    api.recvSelectResult()

    while True:
        api.sendAction("N")
        if api.recvResults()["ended"]:
            break
        api.sendPurchase({})
        api.recvPurchaseResult()
        api.sendSurrenderProposition(True)
        api.recvSurrenderProposition()
        api.sendSurrenderAcceptation(True)
        if api.recvResults()["ended"]:
            break

    api.sendReady()
