#include <crypto++/hex.h>
#include <crypto++/sha.h>
#include <istream>
#include "api.hh"

API::API(const std::string& address, const std::string& port)
    : _IOService(), _socket(_IOService), _resolver(_IOService)

{
    boost::asio::connect(_socket, _resolver.resolve({address, port}));
}

Json::Value API::_recvJSON()
{
    boost::asio::streambuf buffer;
    bool parsingSuccessful = false;
    Json::Value data;

    while (!parsingSuccessful)
    {
        std::size_t size = _socket.read_some(buffer.prepare(4096));
        buffer.commit(size);
        std::istream is(&buffer);

        Json::Reader reader;
        parsingSuccessful = reader.parse(is, data);
    }

    return data;
}

void API::_sendJSON(const Json::Value& data)
{
    Json::StyledWriter writer;
    boost::asio::write(_socket, boost::asio::buffer(writer.write(data)));
}


void API::sendConnect(const std::string& login, const std::string& password)
{
    Json::Value data;
    data["type"] = "connect";
    data["login"] = login;

    CryptoPP::SHA512 hash;
    byte digest[CryptoPP::SHA512::DIGESTSIZE];

    std::string token = login + "_thjx_" + password;
    hash.CalculateDigest(digest, (byte*)token.c_str(), token.length());

    CryptoPP::HexEncoder encoder;
    token.clear();
    encoder.Attach(new CryptoPP::StringSink(token));
    encoder.Put(digest, sizeof(digest));
    encoder.MessageEnd();
    data["password"] = token;

    _sendJSON(data);
}

void API::sendSelect(const std::map<char, unsigned>& strategies)
{
    Json::Value data;
    data["type"] = "select";
    data["strategies"] = Json::objectValue;
    for (auto& pairs: strategies)
        data["strategies"][std::string(1, pairs.first)] = pairs.second;
    _sendJSON(data);
}

void API::sendAction(char action)
{
    Json::Value data;
    data["type"] = "action";
    data["action_played"] = std::string(1, action);
    _sendJSON(data);
}

void API::sendPurchase(const std::map<char, unsigned>& strategies)
{
    Json::Value data;
    data["type"] = "purchase";
    data["strategies"] = Json::objectValue;
    for (auto& pairs: strategies)
        data["strategies"][std::string(1, pairs.first)] = pairs.second;
    _sendJSON(data);
}

void API::sendSurrenderProposition(bool value)
{
    Json::Value data;
    data["type"] = "surrender_proposition";
    data["value"] = value;
    _sendJSON(data);
}

void API::sendSurrenderAcceptation(bool value)
{
    Json::Value data;
    data["type"] = "surrender_acceptation";
    data["value"] = value;
    _sendJSON(data);
}

void API::sendReady()
{
    Json::Value data;
    data["type"] = "ready";
    _sendJSON(data);
}


Json::Value API::recvGame()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "game")
        return Json::Value();
    return data;
}

Json::Value API::recvSelectResult()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "select_result")
        return Json::Value();
    return data;
}

Json::Value API::recvResults()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "results")
        return Json::Value();
    return data;
}

Json::Value API::recvPurchaseResult()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "purchase_result")
        return Json::Value();
    return data;
}

bool API::recvSurrenderProposition()
{
    Json::Value data = _recvJSON();
    return data.get("type", "").asString() == "surrender_proposition"
        && data.get("value", false).asBool();
}
