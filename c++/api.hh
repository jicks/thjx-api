#ifndef API_HH_

# define API_HH_

# include <boost/asio.hpp>
# include <map>
# include <string>
# include "json.hh"

class API
{
    public:
        /*
         * Create the API object and connect to the server at the given address
         * and port.
         */
        API(const std::string& address, const std::string& port);


        /*
         * Send a connect message, with the password as a SHA512 hash.
         */
        void sendConnect(const std::string& login, const std::string& password);

        /*
         * Send the selection of strategies to start the game with.
         * The keys should be one of 'W', 'S', 'G', 'E', 'M', 'T', 'N'. The
         * total sum must be equal to 100. It is not verified by the API.
         */
        void sendSelect(const std::map<char, unsigned>& strategies);

        /*
         * Send the next action to play. The char should be one of 'W', 'S',
         * 'G', 'E', 'M', 'T', 'N'. It is not verified by the API.
         */
        void sendAction(char action);

        /*
         * Send an order to purchase the asked strategies. The keys should be
         * one of 'W', 'S', 'G', 'E', 'M', 'T', 'N'. It is not verified by the
         * API.
         */
        void sendPurchase(const std::map<char, unsigned>& strategies);

        /*
         * Send a surrender proposition with the given value.
         */
        void sendSurrenderProposition(bool value);

        /*
         * Send the surrender acceptation to the challenger proposition with
         * the given value.
         */
        void sendSurrenderAcceptation(bool value);

        /*
         * Send a ready message, stating that you are ready for a new match.
         */
        void sendReady();


        /*
         * Receive the game message from the server. It only checks if the JSON
         * object has the correct type, and returns an empty object if it is
         * not the case.
         */
        Json::Value recvGame();

        /*
         * Receive the select result message from the server. It only checks
         * if the JSON object has the correct type, and returns an empty
         * object if it is not the case.
         */
        Json::Value recvSelectResult();

        /*
         * Receive the results message from the server. It only checks if the
         * JSON object has the correct type, and returns an empty object if it
         * is not the case.
         */
        Json::Value recvResults();

        /*
         * Receive the purchase result message from the server. It only checks
         * if the JSON object has the correct type, and returns an empty
         * object if it is not the case.
         */
        Json::Value recvPurchaseResult();

        /*
         * Receive the challenger surrender proposition from the server.
         * It returns true if the object has the correct type and its value is
         * true.
         */
        bool recvSurrenderProposition();

    private:
        Json::Value _recvJSON();
        void _sendJSON(const Json::Value& data);

        boost::asio::io_service _IOService;
        boost::asio::ip::tcp::socket _socket;
        boost::asio::ip::tcp::resolver _resolver;
};

#endif /* !API_HH_ */
