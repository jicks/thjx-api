#include <map>
#include "api.hh"

int main(int argc, char** argv)
{
    if (argc != 5)
        return 1;

    API api(argv[1], argv[2]);
    api.sendConnect(argv[3], argv[4]);
    while (true)
    {
        api.recvGame();
        api.sendSelect({{'N', 100}});
        api.recvSelectResult();

        while (true)
        {
            api.sendAction('N');

            if (api.recvResults().get("ended", true).asBool())
                break;

            api.sendPurchase({});
            api.recvPurchaseResult();

            api.sendSurrenderProposition(true);
            api.recvSurrenderProposition();
            api.sendSurrenderAcceptation(true);

            if (api.recvResults().get("ended", true).asBool())
                break;
        }

        api.sendReady();
    }
    return 0;
}
