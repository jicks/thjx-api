#ifndef API_H_
# define API_H_

# include <stdbool.h>

struct api;

/*
 * Describes the valid strategies. It is used as an index for arrays
 * representing strategies to select or purchase.
 */
enum actions
{
    W = 0,
    S,
    G,
    E,
    M,
    T,
    N
};

struct game
{
    const char* you;
    const char* challenger;
};

struct playerState
{
    char action_played;
    unsigned strategies[7];
    unsigned score;
};

struct results
{
    bool ended;
    bool win;
    const char* action_winner;
    struct playerState challenger;
    struct playerState you;
};

/*
 * Create the API struct and connect to the server at the given address
 * and port.
 */
struct api* initAPI(const char* address, unsigned port);

/*
 * Delete the given API struct. It also closes the connection to the server.
 */
void deleteAPI(struct api* self);

/*
 * Delete the given game struct (and its internal datas).
 */
void deleteGame(struct game* self);

/*
 * Delete the given results struct (and its internal datas).
 */
void deleteResults(struct results* self);


/*
 * Send a connect message, with the password as a SHA512 hash.
 */
void sendConnect(struct api* self, const char* login, const char* password);

/*
 * Send the selection of strategies to start the game with.
 * The array is used according to the actions enum. If you do not want a
 * strategy, you must set its value to 0 (eg. strategies[M] = 0 if you don't
 * want any M strategy).
 * It is not verified by the API.
 */
void sendSelect(struct api* self, const unsigned strategies[7]);

/*
 * Send the next action to play. The char should be one of 'W', 'S',
 * 'G', 'E', 'M', 'T', 'N'. It is not verified by the API.
 */
void sendAction(struct api* self, char action);

/*
 * Send an order to purchase the asked strategies. The array is used according
 * to the actions enum. If you do not want to buy a strategy, you must set its
 * value to 0 (eg. strategies[M] = 0 if you don't want any new M strategy).  It
 * is not verified by the API.
 */
void sendPurchase(struct api* self, const unsigned strategies[7]);

/*
 * Send a surrender proposition with the given value.
 */
void sendSurrenderProposition(struct api* self, bool value);

/*
 * Send the surrender acceptation to the challenger proposition with
 * the given value.
 */
void sendSurrenderAcceptation(struct api* self, bool value);

/*
 * Send a ready message, stating that you are ready for a new match.
 */
void sendReady(struct api* self);


/*
 * Receive the game message from the server. It only checks if the JSON
 * object has the correct type, and returns NULL if it is not the case.
 * It is your responsability to free the returned pointer correctly (by using
 * the deleteGame function).
 */
struct game* recvGame(struct api* self);

/*
 * Receive the select result message from the server. It only checks if the
 * JSON object has the correct type, and returns NULL if it is not the case.
 * It is your responsability to free the returned pointer correctly (by using
 * the deleteResults function).
 * As the results struct is re-used, the non-relevant fields will be set to 0
 * (or NULL).
 */
struct results* recvSelectResult(struct api* self);

/*
 * Receive the results message from the server. It only checks if the JSON
 * object has the correct type, and returns NULL if it is not the case.
 * It is your responsability to free the returned pointer correctly (by using
 * the deleteResults function).
 */
struct results* recvResults(struct api* self);

/*
 * Receive the purchase result message from the server. It only checks if the
 * JSON object has the correct type, and returns NULL if it is not the case.
 * It is your responsability to free the returned pointer correctly (by using
 * the deleteResults function).
 * As the results struct is re-used, the non-relevant fields will be set to 0
 * (or NULL).
 */
struct results* recvPurchaseResult(struct api* self);

/*
 * Receive the challenger surrender proposition from the server.
 * It returns true if the object has the correct type and its value is
 * true.
 */
bool recvSurrenderProposition(struct api* self);

#endif /* !API_H_ */
