#define _POSIX_C_SOURCE 200809L

#include <arpa/inet.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "api.h"
#include "json.h"


struct api
{
    int socket;
};

struct api* initAPI(const char* address, unsigned port)
{
    struct api* api = malloc(sizeof (api));
    api->socket = 1;

    struct sockaddr_in  sockaddr;

    memset(&sockaddr, 0, sizeof (sockaddr));
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_addr.s_addr = inet_addr(address);
    sockaddr.sin_port = htons(port);

    if ((api->socket = socket(AF_INET, SOCK_STREAM, 0)) < 0 ||
        connect(api->socket, (struct sockaddr*)&sockaddr, sizeof (sockaddr)) < 0)
    {
        free(api);
        return NULL;
    }

    return api;
}

void deleteAPI(struct api* self)
{
    close(self->socket);
    free(self);
}

void deleteGame(struct game* self)
{
    free((char*)self->you);
    free((char*)self->challenger);
    free(self);
}

void deleteResults(struct results* self)
{
    free((char*)self->action_winner);
    free(self);
}


void _sendJSON(struct api* self, const char* datas)
{
    write(self->socket, datas, strlen(datas));
}

json_value* _recvJSON(struct api* self)
{
    unsigned size = 4096;
    unsigned pos = 0;
    char* buffer = malloc(size * sizeof (char));
    json_value* data = NULL;

    while (true)
    {
        int res = read(self->socket, buffer + pos, size - pos - 1);
        if (res <= 0) break;
        pos += res;
        buffer[size - 1] = 0;
        data = json_parse(buffer, pos);
        if (data) break;
        if (pos >= size - 1)
        {
            size *= 2;
            buffer = realloc(buffer, size);
        }
    }

    free(buffer);
    return data;
}


void sendConnect(struct api* self, const char* login, const char* password)
{
    unsigned size = strlen(login) + 6 + strlen(password);
    if (size > 128) size = 128;

    char token[128];
    snprintf(token, size, "%s_thjx_%s", login, password);

    unsigned char hash[SHA512_DIGEST_LENGTH];
    SHA512((unsigned char*)token, size, hash);

    for (unsigned i = 0; i < SHA512_DIGEST_LENGTH; i++)
        sprintf(token + 2 * i, "%.2hhX", hash[i]);

    char datas[256];
    snprintf(datas, 256, "{\"type\": \"connect\", \"login\": \"%s\", \"password\": \"%128s\"}",
            login, token);
    _sendJSON(self, datas);
}

void sendSelect(struct api* self, const unsigned s[7])
{
    char datas[256];
    snprintf(datas, 256, "{\"type\": \"select\", \"strategies\": {\"W\":%u, "
            "\"S\":%u, \"G\":%u, \"E\":%u, \"M\":%u, \"T\":%u, \"N\":%u}}",
            s[0], s[1], s[2], s[3], s[4], s[5], s[6]);
    _sendJSON(self, datas);
}

void sendAction(struct api* self, char action)
{
    char datas[64];
    snprintf(datas, 64, "{\"type\": \"action\", \"action_played\": \"%c\"}", action);
    _sendJSON(self, datas);
}

void sendPurchase(struct api* self, const unsigned s[7])
{
    char datas[256];
    snprintf(datas, 256, "{\"type\": \"purchase\", \"strategies\": {\"W\":%u, "
            "\"S\":%u, \"G\":%u, \"E\":%u, \"M\":%u, \"T\":%u, \"N\":%u}}",
            s[0], s[1], s[2], s[3], s[4], s[5], s[6]);
    _sendJSON(self, datas);
}

void sendSurrenderProposition(struct api* self, bool value)
{
    char datas[64];
    snprintf(datas, 64, "{\"type\": \"surrender_proposition\", \"value\": %s}",
            (value) ? "true" : "false");
    _sendJSON(self, datas);
}

void sendSurrenderAcceptation(struct api* self, bool value)
{
    char datas[64];
    snprintf(datas, 64, "{\"type\": \"surrender_acceptation\", \"value\": %s}",
            (value) ? "true" : "false");
    _sendJSON(self, datas);
}

void sendReady(struct api* self)
{
    char datas[32];
    snprintf(datas, 32, "{\"type\": \"ready\"}");
    _sendJSON(self, datas);
}

struct game* recvGame(struct api* self)
{
    json_value* data = _recvJSON(self);
    if (!data) return NULL;
    struct game* game = calloc(1, sizeof (struct game));
    bool valid = false;

    for (unsigned i = 0; i < data->u.object.length; i++)
    {
        if (strcmp(data->u.object.values[i].name, "type") == 0)
            valid = !strcmp(data->u.object.values[i].value->u.string.ptr, "game");
        else if (strcmp(data->u.object.values[i].name, "you") == 0)
            game->you = strdup(data->u.object.values[i].value->u.string.ptr);
        else if (strcmp(data->u.object.values[i].name, "challenger") == 0)
            game->challenger = strdup(data->u.object.values[i].value->u.string.ptr);
    }

    json_value_free(data);
    if (!valid)
    {
        free(game);
        return NULL;
    }
    return game;
}

static void parsePlayerState(struct playerState* state, json_value* data)
{
    for (unsigned i = 0; i < data->u.object.length; i++)
    {
        if (strcmp(data->u.object.values[i].name, "action_played") == 0)
            state->action_played = data->u.object.values[i].value->u.string.ptr[0];
        else if (strcmp(data->u.object.values[i].name, "score") == 0)
            state->score = data->u.object.values[i].value->u.integer;
        else if (strcmp(data->u.object.values[i].name, "strategies") == 0)
        {
            json_value* object = data->u.object.values[i].value;
            for (unsigned j = 0; j < object->u.object.length; j++)
            {
                unsigned k = 0;
                switch (object->u.object.values[j].name[0])
                {
                    case 'W': k = W; break;
                    case 'S': k = S; break;
                    case 'G': k = G; break;
                    case 'E': k = E; break;
                    case 'M': k = M; break;
                    case 'T': k = T; break;
                    case 'N': k = N; break;
                }
                state->strategies[k] = object->u.object.values[j].value->u.integer;
            }
        }
    }
}

static struct results* parseExpectedResults(struct api* self, const char* type)
{
    json_value* data = _recvJSON(self);
    if (!data) return NULL;
    struct results* results = calloc(1, sizeof (struct results));
    bool valid = false;

    for (unsigned i = 0; i < data->u.object.length; i++)
    {
        if (strcmp(data->u.object.values[i].name, "type") == 0)
            valid = !strcmp(data->u.object.values[i].value->u.string.ptr, type);
        else if (strcmp(data->u.object.values[i].name, "ended") == 0)
            results->ended = data->u.object.values[i].value->u.boolean;
        else if (strcmp(data->u.object.values[i].name, "win") == 0)
            results->win = data->u.object.values[i].value->u.boolean;
        else if (strcmp(data->u.object.values[i].name, "action_winner") == 0)
            results->action_winner = strdup(data->u.object.values[i].value->u.string.ptr);
        else if (strcmp(data->u.object.values[i].name, "you") == 0)
            parsePlayerState(&results->you, data->u.object.values[i].value);
        else if (strcmp(data->u.object.values[i].name, "challenger") == 0)
            parsePlayerState(&results->challenger, data->u.object.values[i].value);
    }

    json_value_free(data);
    if (!valid)
    {
        free(results);
        return NULL;
    }
    return results;
}

struct results* recvSelectResult(struct api* self)
{
    return parseExpectedResults(self, "select_result");
}

struct results* recvResults(struct api* self)
{
    return parseExpectedResults(self, "results");
}

struct results* recvPurchaseResult(struct api* self)
{
    return parseExpectedResults(self, "purchase_result");
}

bool recvSurrenderProposition(struct api* self)
{
    json_value* data = _recvJSON(self);
    if (!data) return false;
    bool valid = false;
    bool value = false;

    for (unsigned i = 0; i < data->u.object.length; i++)
    {
        if (strcmp(data->u.object.values[i].name, "type") == 0)
            valid = !strcmp(data->u.object.values[i].value->u.string.ptr,
                    "surrender_proposition");
        else if (strcmp(data->u.object.values[i].name, "value") == 0)
            value = data->u.object.values[i].value->u.boolean;
    }

    json_value_free(data);
    return valid && value;
}
