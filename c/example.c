#include <stdlib.h>

#include "api.h"

int main(int argc, char** argv)
{
    if (argc != 5)
        return 1;

    struct api* api = initAPI(argv[1], atoi(argv[2]));
    sendConnect(api, argv[3], argv[4]);
    while (true)
    {
        struct game* game = recvGame(api);
        unsigned strategies[7] = {0, 0, 0, 0, 0, 0, 100};
        sendSelect(api, strategies);
        strategies[6] = 0;
        deleteResults(recvSelectResult(api));

        while (true)
        {
            sendAction(api, 'N');

            struct results* results = recvResults(api);
            bool ended = results->ended;
            deleteResults(results);
            if (ended) break;

            sendPurchase(api, strategies);
            deleteResults(recvPurchaseResult(api));

            sendSurrenderProposition(api, true);
            recvSurrenderProposition(api);
            sendSurrenderAcceptation(api, true);

            results = recvResults(api);
            ended = results->ended;
            deleteResults(results);
            if (ended) break;
        }

        deleteGame(game);
        sendReady(api);
    }
    deleteAPI(api);
    return 0;
}
