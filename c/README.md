What is it?
===========

A simple C API for the THJX project. It is functional and easy to use, but it
is very limited (for example, there is *NO* data validation).


How to use it?
==============

Juste run `make all` to obtain the 'libapi.a'.
You can compile the example with `make test`. This example ('example.c') is
intended to show you how to use the API.

The API uses several libraries:

*   [json-parser](https://github.com/udp/json-parser), a very good JSON parser
    in ANSI C. It is already in the tarball so you don't have to install
    anything;
*   [OpenSSL crypto](http://www.openssl.org/docs/crypto/crypto.html), a
    well-known cryptographic library (used for the SHA512). On a Linux distro,
    the package name is probably something like 'libssl-dev' or 'openssl-dev'.

The documentation is the comments in 'api.h'. It is the only header you have to
read. For more information on the message exchange, please read the full
specifications.


Disclaimer
==========

This API is only an example, to help you start your project. You do not have to
use it, you can reimplement it if you wish.

There is *no* warranty of the validity of this API. It has been tested, but
some bugs may remain, they will probably not be fixed.
