␖Valid messages:
=============

*   Connection from a player (the password to send is the SHA512 of
    login+"_thjx_"+password, in uppercased hexadecimal)

        {
          "type": "connect",
          "login": "login_",
          "password": "..." // sha512(login+"_thjx_"+password).hexdigest().upper()
        }

*   Strategies selection result from the server

        {
          "type": "select_result",
          "you": {
              "strategies": {"W":9 ......}
          },
          "challenger": {
              "strategies": {"W":9 ......}
          }
        }

*   New Game from the server

        {
          "type": "game",
           "you": "login_x",
           "challenger": "login_y"
        }

*   Selection of the strategies from a player at the beginning of a party

        {
          "type": "select",
          "strategies": {"W":10, "S":1, "G":X, "E":X, "M":X, "T":X, "N":X}
        }


*   Action from a player

        {
          "type": "action",
          "action_played": "N" // OR W, S, G, E, M, T
        }

*   Results of the actions from the server

        {
          "type": "results",
          "ended": false,
          "win": false,
          "action_winner": "login_x",
          "challenger": {
            "action_played": "N",
            "strategies": {"W":9 ......},
            "score": 100
          },
          "you": {
            "action_played": "X",
            "strategies": {"W":9 ......},
            "score": 30
          }
        }


*   Purchase from a player

        {
          "type": "purchase",
          "strategies": {"W": 1, ...},
        }


*   Purchase completion from the server

        {
          "type": "purchase_result",
          "you": {
            "strategies": {"W":9 ......},
            "score": 100 // Updated
          },
          "challenger": {
            "strategies": {"W":9 ......},
            "score": 100 // Updated
          }
        }

*   Surrender proposition from a player or Surrender proposition of the challenger from the server (same!)

        {
          "type": "surrender_proposition",
          "value": true
        }

*   Surrender acceptation from a player

        {
          "type": "surrender_acceptation",
          "value": true
        }

*   Results of the game from the server

        {
          "type": "results",
          "ended": false,
          "win": false
        }

*   A player is ready for another game

        {
          "type": "ready"
        }

Typical messages flow:
===================

1. Only once (at the beginning):

  * Player => CONNECT

2. At the beginning of each game:

  * Server <= GAME

  * Player => SELECT

  * Server <= SELECT_RESULT

3. At each game turn:

  * Player => ACTION

  * Server <= RESULTS (possible ending here)

  * Player => PURCHASE

  * Server <= PURCHASE_RESULT

  * Player => SURRENDER_PROPOSITION

  * Server <= SURRENDER_PROPOSITION (from the challenger)

  * Player => SURRENDER_ACCEPTATION

  * Server <= RESULTS (possible ending here)

4. At the end of each game:

  * Player => READY (when he is ready for a new game)
